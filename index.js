const { log, objects } = require('./utils');
const data = require('./data');

const keys = Object
  .keys(data)
  .map(keyList => keyList.split('.'))
  .map(keyList => objects.fromKeys(keyList, data[keyList.join('.')]))
  .reduce((result, current) => {
    return objects.merge(result, current);
  }, {});

log(keys);
