const _ = require('lodash');

function log(data) {
  console.dir(data, { depth: null });
}

function objectFromKeys(keys, value) {
  if (keys.length === 1) {
    return {
      [keys[0]]: value,
    };
  }
  const key = keys.shift();
  return {
    [key]: objectFromKeys(keys, value),
  };
}

function mergeObjects(obj1, obj2) {
  return _.merge(obj1, obj2);
}

module.exports = {
  log,
  objects: {
    fromKeys: objectFromKeys,
    merge: mergeObjects,
  },
  regex: {
    hasArray: /(?<=\[)(.*)(?=\])/,
  },
};
