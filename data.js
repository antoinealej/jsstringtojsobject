const data = {
  'user.lastName': 'Alejandro',
  'user.firstName': 'Antoine',
  'user.age': 30,
  'pets[0].name.firstName': 'Baloo',
  'pets[0].name.lastName': 'Alejandro',
  'pets[0].age': 3,
  'pets[1].name': 'Gribouille',
  'pets[1].age': 10,
  'countries.France.cities[0].name': 'Tours',
  'countries.France.cities[0].population': 136252,
  'countries.France.cities[3].name': 'Sonzay',
  'countries.France.cities[3].population': 1403,
};

module.exports = data;
